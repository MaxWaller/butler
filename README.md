# butler

![repo-banner](BUTLER.png)

Data hydration for server-side rendering powered by [React Query](https://github.com/tannerlinsley/react-query).

TODO: Fill out this long description.
This package provides a cache for server-side rendering and data serialization for sending client applications.
It exports all modules from [React Query](https://github.com/tannerlinsley/react-query) so you don't need to install that too.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

## Install

```
#yarn
yarn add https://bitbucket.org/MaxWaller/butler.git#semver:^0

#npm
npm install https://bitbucket.org/MaxWaller/butler.git#semver:^0

```

## Usage

To use Butler inside a React app you must wrap it inside a react-query cache provider.
```tsx
import { Butler as ButlerCache, ButlerProvider, makeQueryCache, ReactQueryCacheProvider } from 'butler';

const INITIAL_STATE = window.__STATE__ || {};
delete window.__STATE__;
const butler = new ButlerCache();
butler.loadCache(INITIAL_STATE);
const queryCache = makeQueryCache();


...
// Inside the root component
<ReactQueryCacheProvider queryCache={queryCache}>
    <ButlerProvider butler={butler}>
        <MyApp />
    </ButlerProvider>
</ReactQueryCacheProvider>
```

Use butler just like you would React-query but with the useCacheQuery instead for cached results
```tsx
import { useCacheQuery } from 'butler';
...
// Inside a react component you call
// handleRequest is a function that fetches data and returns it
const projectQuery = useCacheQuery(['project', id], handleRequest);
```

## API

## Maintainers

[MaxWaller](https://bitbucket.com/MaxWaller)

## Contributing



Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Max Waller
