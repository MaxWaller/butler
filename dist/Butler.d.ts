export declare type QueryTransform<TResult> = (result: TResult) => TResult;
declare type ButlerFunction<TResult> = (...args: any) => Promise<TResult>;
declare type ButlerCache = {
    [key: string]: any;
};
export default class Butler {
    readonly isServerSide: boolean;
    private queue;
    private internalDataCache;
    private internalErrorCache;
    constructor(config?: {
        isServerSide: boolean;
    });
    cache<TResult = any>(queryKey: any, queryFn: ButlerFunction<TResult>, queryTransform?: QueryTransform<TResult>): TResult;
    caching(): Promise<boolean>;
    isCaching(): number;
    serializeCache(): {
        data: ButlerCache;
    };
    loadCache(cache: {
        data: ButlerCache;
    }): void;
    private addToCacheQueue;
}
export {};
