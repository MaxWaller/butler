import React from 'react';
import Butler from './butler';
interface ButlerProviderProps {
    butler: Butler;
}
export declare const ButlerProvider: React.FunctionComponent<ButlerProviderProps>;
export declare const useButler: () => Butler;
export {};
