import { useQuery, usePaginatedQuery, useInfiniteQuery } from 'react-query';
export * from 'react-query';
import React, { useContext, createContext, useMemo } from 'react';

// A type of promise-like that resolves synchronously and supports only one observer

const _iteratorSymbol = /*#__PURE__*/ typeof Symbol !== "undefined" ? (Symbol.iterator || (Symbol.iterator = Symbol("Symbol.iterator"))) : "@@iterator";

const _asyncIteratorSymbol = /*#__PURE__*/ typeof Symbol !== "undefined" ? (Symbol.asyncIterator || (Symbol.asyncIterator = Symbol("Symbol.asyncIterator"))) : "@@asyncIterator";

// Asynchronously call a function and send errors to recovery continuation
function _catch(body, recover) {
	try {
		var result = body();
	} catch(e) {
		return recover(e);
	}
	if (result && result.then) {
		return result.then(void 0, recover);
	}
	return result;
}

/*
MIT License

Copyright (c) 2019 Tanner Linsley

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
// From: https://github.com/tannerlinsley/react-query
function isObject(a) {
  return a && typeof a === 'object' && !Array.isArray(a);
}

function stableStringifyReplacer(_, value) {
  return isObject(value) ? Object.assign.apply(Object, [{}].concat(Object.keys(value).sort().map(function (key) {
    var _ref;

    return _ref = {}, _ref[key] = value[key], _ref;
  }))) : value;
}

function stableStringify(obj) {
  return JSON.stringify(obj, stableStringifyReplacer);
}

function queryKeySerializer(queryKey) {
  try {
    var arrayQueryKey = Array.isArray(queryKey) ? queryKey : [queryKey];
    var queryHash = stableStringify(arrayQueryKey);
    arrayQueryKey = JSON.parse(queryHash);
    return [queryHash, arrayQueryKey];
  } catch (_unused) {
    throw new Error('A valid query key is required!');
  }
}

var Butler = /*#__PURE__*/function () {
  function Butler(config) {
    var _this = this;

    if (config === void 0) {
      config = {
        isServerSide: typeof window === 'undefined'
      };
    }

    this.isServerSide = config.isServerSide;
    this.queue = {};
    this.internalDataCache = {};
    this.internalErrorCache = {};

    if (!this.isServerSide) {
      // invalidate the cache after 500 ms if not on the server
      setTimeout(function () {
        _this.internalDataCache = {};
        _this.internalErrorCache = {};
      }, 500);
    }
  }

  var _proto = Butler.prototype;

  _proto.cache = function cache(queryKey, queryFn, queryTransform) {
    var _queryKeySerializer = queryKeySerializer(queryKey),
        cacheKey = _queryKeySerializer[0];

    var cacheResult = this.internalDataCache[cacheKey];

    if (cacheResult) {
      if (queryTransform) {
        return queryTransform(cacheResult);
      } else {
        return cacheResult;
      }
    } // How do we handle errors?


    var cacheError = this.internalErrorCache[cacheKey];

    if (cacheError) {
      // @ts-ignore
      return undefined;
    }

    if (this.isServerSide) {
      var parameters = [];

      if (Array.isArray(queryKey)) {
        // Skip the first parameter
        parameters = queryKey.slice(1);
      } // If we are already in the queue, then wait


      if (this.queue[cacheKey]) {
        // @ts-ignore
        return undefined;
      }

      this.addToCacheQueue(cacheKey, queryFn.apply(void 0, [this].concat(parameters)));
    } // @ts-ignore


    return undefined;
  } // Server side rendering functions
  ;

  _proto.caching = function caching() {
    try {
      var _this3 = this;

      var doneCount = 0;
      var totalCount = 0;

      var awaitAll = function awaitAll() {
        try {
          var current = Object.values(_this3.queue);
          _this3.queue = {};
          return Promise.resolve(Promise.all(current)).then(function () {});
        } catch (e) {
          return Promise.reject(e);
        }
      };

      return Promise.resolve(awaitAll()).then(function () {
        return totalCount !== doneCount;
      });
    } catch (e) {
      return Promise.reject(e);
    }
  };

  _proto.isCaching = function isCaching() {
    return Object.values(this.queue).length;
  };

  _proto.serializeCache = function serializeCache() {
    return {
      data: this.internalDataCache
    };
  };

  _proto.loadCache = function loadCache(cache) {
    if (cache.data) {
      this.internalDataCache = cache.data;
    }
  };

  _proto.addToCacheQueue = function addToCacheQueue(cacheKey, awaitingPromise) {
    var _this4 = this;

    var queuedPromise = new Promise(function (resolve) {
      try {
        var _temp2 = _catch(function () {
          return Promise.resolve(awaitingPromise).then(function (result) {
            _this4.internalDataCache[cacheKey] = result;
            resolve();
          });
        }, function (e) {
          _this4.internalErrorCache[cacheKey] = e;
          resolve();
        });

        return Promise.resolve(_temp2 && _temp2.then ? _temp2.then(function () {}) : void 0);
      } catch (e) {
        return Promise.reject(e);
      }
    });
    this.queue[cacheKey] = queuedPromise;
  };

  return Butler;
}();

var ButlerContext = /*#__PURE__*/createContext(undefined);
var ButlerProvider = function ButlerProvider(_ref) {
  var butler = _ref.butler,
      children = _ref.children;
  return React.createElement(ButlerContext.Provider, {
    value: butler
  }, children);
};
var useButler = function useButler() {
  return useContext(ButlerContext);
};

function getArgs(args) {
  var queryKey = args[0],
      queryFn = args[1],
      _args$ = args[2],
      transformFn = _args$ === void 0 ? {} : _args$,
      _args$2 = args[3],
      config = _args$2 === void 0 ? {} : _args$2;
  var queryFnWithTransform = queryFn;

  if (typeof transformFn === 'function') {
    queryFnWithTransform = function queryFnWithTransform() {
      try {
        var _transformFn2 = transformFn;
        return Promise.resolve(queryFn.apply(void 0, arguments)).then(_transformFn2);
      } catch (e) {
        return Promise.reject(e);
      }
    };
  } else {
    config = transformFn;
    transformFn = undefined;
  }

  return [queryKey, queryFn, transformFn, queryFnWithTransform, config];
}

/*
    This function fetches or returns data from the cache
 */

function useCache(queryKey, queryFn, queryTransform) {
  var butler = useButler();
  var initialData = useMemo(function () {
    return butler.cache(queryKey, queryFn, queryTransform);
  }, // eslint-disable-next-line react-hooks/exhaustive-deps
  []);
  return initialData;
}
function useCacheQuery() {
  var butler = useButler();

  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  var _getArgs = getArgs(args),
      queryKey = _getArgs[0],
      queryFn = _getArgs[1],
      transformFn = _getArgs[2],
      queryFnWithTransform = _getArgs[3],
      config = _getArgs[4];

  if (!config.initialData) {
    config.initialData = function () {
      return butler.cache(queryKey, queryFn, transformFn);
    };
  }

  var queryResult = useQuery(queryKey, queryFnWithTransform, config);
  return queryResult;
}
function useCachePaginatedQuery() {
  var butler = useButler();

  for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    args[_key2] = arguments[_key2];
  }

  var _getArgs2 = getArgs(args),
      queryKey = _getArgs2[0],
      queryFn = _getArgs2[1],
      transformFn = _getArgs2[2],
      queryFnWithTransform = _getArgs2[3],
      config = _getArgs2[4];

  if (!config.initialData) {
    config.initialData = function () {
      return butler.cache(queryKey, queryFn, transformFn);
    };
  }

  var queryResult = usePaginatedQuery(queryKey, queryFnWithTransform, config);
  return queryResult;
}
function useCacheInfiniteQuery() {
  var butler = useButler();

  for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    args[_key3] = arguments[_key3];
  }

  var _getArgs3 = getArgs(args),
      queryKey = _getArgs3[0],
      queryFn = _getArgs3[1],
      transformFn = _getArgs3[2],
      queryFnWithTransform = _getArgs3[3],
      config = _getArgs3[4];

  if (!config.initialData) {
    config.initialData = function () {
      return butler.cache(queryKey, queryFn, transformFn);
    };
  }

  var queryResult = useInfiniteQuery(queryKey, queryFnWithTransform, config);
  return queryResult;
}

var butlerRender = function butlerRender(renderFunction, butlerCache, options) {
  if (options === void 0) {
    options = {};
  }

  try {
    var _options$timeout;

    var timeout = (_options$timeout = options.timeout) !== null && _options$timeout !== void 0 ? _options$timeout : 500;
    var startTime = Date.now();

    var process = function process() {
      try {
        var _exit2 = false;
        var result = renderFunction();
        var endTime = Date.now() - startTime;

        var _temp2 = function () {
          if (butlerCache.isCaching()) {
            return Promise.resolve(butlerCache.caching()).then(function () {
              if (endTime < timeout) {
                _exit2 = true;
                return process();
              }
            });
          }
        }();

        return Promise.resolve(_temp2 && _temp2.then ? _temp2.then(function (_result) {
          return _exit2 ? _result : result;
        }) : _exit2 ? _temp2 : result);
      } catch (e) {
        return Promise.reject(e);
      }
    };

    return process();
  } catch (e) {
    return Promise.reject(e);
  }
};

export { Butler, ButlerProvider, butlerRender, useButler, useCache, useCacheInfiniteQuery, useCachePaginatedQuery, useCacheQuery };
//# sourceMappingURL=butler.esm.js.map
