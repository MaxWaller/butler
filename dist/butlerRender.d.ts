import Butler from './butler';
declare type RenderFunction<R = Record<any, any>> = () => R;
declare type RenderOptions = {
    timeout?: number;
};
declare function butlerRender<R = Record<any, any>>(renderFunction: RenderFunction<R>, butlerCache: Butler, options?: RenderOptions): Promise<R>;
export default butlerRender;
