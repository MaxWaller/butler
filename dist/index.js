
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./butler.cjs.production.min.js')
} else {
  module.exports = require('./butler.cjs.development.js')
}
