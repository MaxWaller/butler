import { Butler, useQueryCache } from '../src';

describe('Butler', () => {
  it('can instantiate Butler', () => {
    const butler = new Butler();
    expect(butler instanceof Butler).toBeTruthy();
  });
  it('can instantiate useQueryCache', () => {
    expect(useQueryCache).toBeTruthy();
  });
});
