import React, { createContext, useContext } from 'react';
import Butler from './butler';

// @ts-ignore
const ButlerContext = createContext<Butler>(undefined);

interface ButlerProviderProps {
  butler: Butler;
}

export const ButlerProvider: React.FunctionComponent<ButlerProviderProps> = ({
  butler,
  children,
}) => {
  return (
    <ButlerContext.Provider value={butler}>{children}</ButlerContext.Provider>
  );
};

export const useButler: () => Butler = () => {
  return useContext(ButlerContext);
};
