import queryKeySerializer from './queryKeySerializer';

type CacheKey = string;
export type QueryTransform<TResult> = (result: TResult) => TResult;
type ButlerFunction<TResult> = (...args: any) => Promise<TResult>;
type ButlerQueue = { [key: string]: Promise<void> };
type ButlerErrorCache = { [key: string]: any };
type ButlerCache = { [key: string]: any };

export default class Butler {
  public readonly isServerSide: boolean;
  private queue: ButlerQueue;
  private internalDataCache: ButlerCache;
  private internalErrorCache: ButlerErrorCache;

  public constructor(config = { isServerSide: typeof window === 'undefined' }) {
    this.isServerSide = config.isServerSide;
    this.queue = {};
    this.internalDataCache = {};
    this.internalErrorCache = {};
    if (!this.isServerSide) {
      // invalidate the cache after 500 ms if not on the server
      setTimeout(() => {
        this.internalDataCache = {};
        this.internalErrorCache = {};
      }, 500);
    }
  }

  public cache<TResult = any>(
    queryKey: any,
    queryFn: ButlerFunction<TResult>,
    queryTransform?: QueryTransform<TResult>
  ): TResult {
    const [cacheKey] = queryKeySerializer(queryKey);
    const cacheResult = this.internalDataCache[cacheKey];
    if (cacheResult) {
      if (queryTransform) {
        return queryTransform(cacheResult);
      } else {
        return cacheResult;
      }
    }
    // How do we handle errors?
    const cacheError = this.internalErrorCache[cacheKey];
    if (cacheError) {
      // @ts-ignore
      return undefined;
    }

    if (this.isServerSide) {
      let parameters = [];
      if (Array.isArray(queryKey)) {
        // Skip the first parameter
        [, ...parameters] = queryKey;
      }
      // If we are already in the queue, then wait
      if (this.queue[cacheKey]) {
        // @ts-ignore
        return undefined;
      }
      this.addToCacheQueue(cacheKey, queryFn(this, ...parameters));
    }
    // @ts-ignore
    return undefined;
  }

  // Server side rendering functions
  public async caching() {
    let doneCount = 0;
    let totalCount = 0;
    const awaitAll = async () => {
      const current = Object.values(this.queue);
      this.queue = {};
      await Promise.all(current);
    };
    await awaitAll();
    return totalCount !== doneCount;
  }

  public isCaching(): number {
    return Object.values(this.queue).length;
  }

  public serializeCache(): { data: ButlerCache } {
    return {
      data: this.internalDataCache,
    };
  }

  public loadCache(cache: { data: ButlerCache }) {
    if (cache.data) {
      this.internalDataCache = cache.data;
    }
  }

  private addToCacheQueue(cacheKey: CacheKey, awaitingPromise: Promise<any>) {
    const queuedPromise: Promise<void> = new Promise(async resolve => {
      try {
        const result = await awaitingPromise;
        this.internalDataCache[cacheKey] = result;
        resolve();
      } catch (e) {
        this.internalErrorCache[cacheKey] = e;
        resolve();
      }
    });
    this.queue[cacheKey] = queuedPromise;
  }
}
