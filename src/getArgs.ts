export default function getArgs(args: any[]) {
  let [queryKey, queryFn, transformFn = {}, config = {}] = args;
  let queryFnWithTransform = queryFn;
  if (typeof transformFn === 'function') {
    queryFnWithTransform = async (...args: any[]) =>
      transformFn(await queryFn(...args));
  } else {
    config = transformFn;
    transformFn = undefined;
  }

  return [queryKey, queryFn, transformFn, queryFnWithTransform, config];
}
