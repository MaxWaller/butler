import Butler from './butler';

type RenderFunctionPromise<R = Record<any, any>> = () => Promise<R>;
type RenderFunction<R = Record<any, any>> = () => R;
type RenderOptions = {
  timeout?: number;
};

async function butlerRender<R = Record<any, any>>(
  renderFunction: RenderFunction<R>,
  butlerCache: Butler,
  options: RenderOptions = {}
): Promise<R> {
  const timeout = options.timeout ?? 500;
  const startTime = Date.now();
  const process: RenderFunctionPromise<R> = async () => {
    const result = renderFunction();
    const endTime = Date.now() - startTime;
    if (butlerCache.isCaching()) {
      await butlerCache.caching();
      if (endTime < timeout) {
        return process();
      }
    }
    return result;
  };
  return process();
}

export default butlerRender;
