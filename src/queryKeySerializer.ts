/*
MIT License

Copyright (c) 2019 Tanner Linsley

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
// From: https://github.com/tannerlinsley/react-query

function isObject(a: any) {
  return a && typeof a === 'object' && !Array.isArray(a);
}
function stableStringifyReplacer(_: unknown, value: any) {
  return isObject(value)
    ? Object.assign(
        {},
        ...Object.keys(value)
          .sort()
          .map(key => ({
            [key]: value[key],
          }))
      )
    : value;
}

function stableStringify(obj: any) {
  return JSON.stringify(obj, stableStringifyReplacer);
}
function queryKeySerializer(queryKey: any): [string, string[]] {
  try {
    let arrayQueryKey = Array.isArray(queryKey) ? queryKey : [queryKey];
    const queryHash = stableStringify(arrayQueryKey);
    arrayQueryKey = JSON.parse(queryHash);
    return [queryHash, arrayQueryKey];
  } catch {
    throw new Error('A valid query key is required!');
  }
}

export default queryKeySerializer;
