import { useMemo } from 'react';
import {
  AnyQueryKey,
  QueryFunction,
  QueryOptions,
  QueryResult,
  PaginatedQueryResult,
  usePaginatedQuery,
  useQuery,
  useInfiniteQuery,
  InfiniteQueryResult,
  InfiniteQueryKey,
  InfiniteQueryFunction,
  InfiniteQueryOptions,
} from 'react-query';
import { QueryTransform } from './butler';
import getArgs from './getArgs';
import { useButler } from './useButler';

/*
    This function fetches or returns data from the cache
 */
export function useCache<TResult>(
  queryKey: any,
  queryFn: any,
  queryTransform?: QueryTransform<TResult>
): TResult {
  const butler = useButler();
  let initialData = useMemo(
    () => butler.cache<TResult>(queryKey, queryFn, queryTransform),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );
  return initialData;
}
// Overload useCacheQuery
export type UseCacheQueryRest<TResult, TKey extends AnyQueryKey, TError> =
  | [QueryFunction<TResult, TKey>]
  | [QueryFunction<TResult, TKey>, QueryTransform<TResult>]
  | [QueryFunction<TResult, TKey>, QueryOptions<TResult, TError>]
  | [
      QueryFunction<TResult, TKey>,
      QueryTransform<TResult>,
      QueryOptions<TResult, TError>
    ];
export function useCacheQuery<
  TResult,
  TKey extends AnyQueryKey,
  TError = Error
>(
  queryKey: TKey | false | null | undefined,
  ...rest: UseCacheQueryRest<TResult, TKey, TError>
): QueryResult<TResult, TError>;
export function useCacheQuery<TResult, TKey extends string, TError = Error>(
  queryKey: TKey | false | null | undefined,
  ...rest: UseCacheQueryRest<TResult, [TKey], TError>
): QueryResult<TResult, TError>;

export function useCacheQuery(...args: any[]) {
  const butler = useButler();
  const [
    queryKey,
    queryFn,
    transformFn,
    queryFnWithTransform,
    config,
  ] = getArgs(args);

  if (!config.initialData) {
    config.initialData = () => butler.cache(queryKey, queryFn, transformFn);
  }
  const queryResult = useQuery(queryKey, queryFnWithTransform, config);
  return queryResult;
}

// Overload useCachePaginatedQuery
export function useCachePaginatedQuery<
  TResult,
  TKey extends AnyQueryKey,
  TError = Error
>(
  queryKey: TKey | false | null | undefined,
  ...rest: UseCacheQueryRest<TResult, TKey, TError>
): PaginatedQueryResult<TResult, TError>;
export function useCachePaginatedQuery<
  TResult,
  TKey extends string,
  TError = Error
>(
  queryKey: TKey | false | null | undefined,
  ...rest: UseCacheQueryRest<TResult, [TKey], TError>
): PaginatedQueryResult<TResult, TError>;
export function useCachePaginatedQuery(...args: any[]) {
  const butler = useButler();
  const [
    queryKey,
    queryFn,
    transformFn,
    queryFnWithTransform,
    config,
  ] = getArgs(args);

  if (!config.initialData) {
    config.initialData = () => butler.cache(queryKey, queryFn, transformFn);
  }
  const queryResult = usePaginatedQuery(queryKey, queryFnWithTransform, config);
  return queryResult;
}

export type UseCacheInfiniteQueryRest<
  TResult,
  TKey extends AnyQueryKey,
  TMoreVariable,
  TError
> =
  | [InfiniteQueryFunction<TResult, TKey, TMoreVariable>]
  | [
      InfiniteQueryFunction<TResult, TKey, TMoreVariable>,
      QueryTransform<TResult>
    ]
  | [
      InfiniteQueryFunction<TResult, TKey, TMoreVariable>,
      InfiniteQueryOptions<TResult, TMoreVariable, TError>
    ]
  | [
      InfiniteQueryFunction<TResult, TKey, TMoreVariable>,
      QueryTransform<TResult>,
      InfiniteQueryOptions<TResult, TMoreVariable, TError>
    ];

// Overload useCacheInfiniteQuery
export function useCacheInfiniteQuery<
  TResult,
  TKey extends AnyQueryKey,
  TMoreVariable,
  TError = Error
>(
  queryKey: InfiniteQueryKey<TKey>,
  ...rest: UseCacheInfiniteQueryRest<TResult, TKey, TMoreVariable, TError>
): InfiniteQueryResult<TResult, TError>;
export function useCacheInfiniteQuery<
  TResult,
  TSingleKey extends string,
  TMoreVariable,
  TError = Error
>(
  queryKey: InfiniteQueryKey<TSingleKey>,
  ...rest: UseCacheInfiniteQueryRest<
    TResult,
    [TSingleKey],
    TMoreVariable,
    TError
  >
): InfiniteQueryResult<TResult, TError>;
export function useCacheInfiniteQuery(...args: any[]) {
  const butler = useButler();
  const [
    queryKey,
    queryFn,
    transformFn,
    queryFnWithTransform,
    config,
  ] = getArgs(args);

  if (!config.initialData) {
    config.initialData = () => butler.cache(queryKey, queryFn, transformFn);
  }
  const queryResult = useInfiniteQuery(queryKey, queryFnWithTransform, config);
  return queryResult;
}
